import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.awt.Image;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class FoodOrderMachine {
    private JPanel root;
    private JLabel topLabel;
    private JLabel orderedItemsLabel;
    private JList<String> orderedItemList;
    private JTextPane totalAmountLabel;
    private JButton checkOutButton;
    private JToolBar menuKindsBar;
    private JButton takeoutMenuButton;
    private JButton setMenuButton;
    private JButton singleMenuButton;
    private JButton kidsMenuButton;
    private JButton takeOutBeefBowlButton;
    private JButton takeOutOkraBeefBowlButton;
    private JButton takeOutSukiyakiBowlButton;
    private JButton setBeefBowlButton;
    private JButton setKidsButton;
    private JButton setCurryButton;
    private JButton kidsCurryButton;
    private JButton kidsYakisobaButton;
    private JButton singleBeefBowlButton;
    private JButton singleChickenCurryButton;
    private JButton singleOroshiButton;
    private JScrollBar scrollBar1;
    private JButton allMenuButton;
    private JButton couponPasswordButton;
    private JSlider couponCodeSlider;
    private JLabel sliderValueLabel;
    private JButton couponCodeConfirmButton;
    private JButton[] menuButtons= {
            //各アイコンに画像を設定するときに一括で設定できるよう配列にまとめる。
            takeOutBeefBowlButton,
            takeOutOkraBeefBowlButton,
            takeOutSukiyakiBowlButton,
            setBeefBowlButton,
            setKidsButton,
            setCurryButton,
            kidsCurryButton,
            kidsYakisobaButton,
            singleBeefBowlButton,
            singleChickenCurryButton,
            singleOroshiButton
    };


    DefaultListModel<String> orderList;
    //public FoodOrderMachine()内で注文された商品をJList orderdItemListに追加する際に、注文された商品名を保持しておく変数

    private Map<String, Integer> menu;
    //商品名と値段を一元管理するため辞書式変数を使用

    int totalPrice = 0;
    //合計金額を格納する変数

    private Map<String, JSpinner> purchaseNumber;
    //商品の購入個数を管理するための辞書式変数 JSpinnerという型は
    //ユーザが設定した購入個数を保持しているものである。

    int couponUsageCount=-1;
    //クーポンを一度使用したら再度は使えないようにするための変数

    private String[] getProductNames = {
            //商品名の一括管理
            "Beef Bowl_take out",
            "Okra Beef Bowl_take out",
            "Sukiyaki Bowl_take out",
            "Beef Bowl_set",
            "Kids set",
            "Curry Set",
            "Kids Curry",
            "Kids Yakisoba",
            "Beef Bowl_single",
            "Chicken Curry_single",
            "Oroshi Ponzu Beef Bowl_single"
    };

    private String[] getProductIconNames = {
            //商品アイコン画像名の一括管理
            "takeout_beefbowl.jpg",
            "takeout_okra.jpg",
            "takeout_sukiyaki.jpg",
            "set_beefbowl.jpg",
            "set_kids.jpg",
            "set_curry.jpg",
            "kids_curry.jpg",
            "kids_yakisoba.jpg",
            "single_beef_bowl.jpg",
            "single_chikincurry.jpg",
            "single_orosi.jpg"
    };

    private Integer[] getProductPrice = {
            //商品値段の一括管理
            590,
            700,
            800,
            680,
            580,
            650,
            500,
            450,
            600,
            750,
            710
    };


    public FoodOrderMachine() {

        //↓必要な変数の宣言,設定↓

        // couponCodeSliderの初期化
        couponCodeSlider = new JSlider(0, 100);
        couponCodeSlider.setMajorTickSpacing(10);
        couponCodeSlider.setMinorTickSpacing(1);
        couponCodeSlider.setPaintLabels(true);

        //スクロールバーの宣言
        JScrollPane scrollPane = new JScrollPane(root, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setVerticalScrollBar(scrollBar1);


        purchaseNumber = new HashMap<>();
        //各商品の購入数を記憶

        orderList = new DefaultListModel<>();
        // この変数に直前に注文した商品の名前を格納
        orderedItemList.setModel(orderList);
        // GUIに表示

        totalAmountLabel.setText("Total : " + totalPrice + " yen");
        //合計金額を表示

        // メニューの商品と価格を一元管理
        menu = new HashMap<>();
        for (int i = 0; i < getProductNames.length; i++) {
            // 商品名と価格をマップに追加
            menu.put(getProductNames[i], getProductPrice[i]);
        }

        //purchaseNumberは各商品の購入個数を商品名をキーにして保持する
        for(String productName:getProductNames) {
            purchaseNumber.put(productName, new JSpinner(new SpinnerNumberModel(1, 0, 20, 1)));
        }

        //各ボタンに画像を設定
        int iconResize=125;//画像アイコンの大きさ,デフォルトだと大きすぎるのでリサイズする。
        setAndResizeAllImageIcon(menuButtons,getProductIconNames,iconResize);

        //各ボタンに商品名と値段を設定
        setButtonText(menuButtons,getProductNames,getProductPrice);


        //↓ボタンの機能の設定↓


        couponCodeSlider.addChangeListener(new ChangeListener() {
            //クーポンコードを入力するために使用するスライダー
            @Override
            public void stateChanged(ChangeEvent e) {

                int sliderValue = couponCodeSlider.getValue();
                sliderValueLabel.setText("Coupon Code Value: " + sliderValue);
            }
        });


        couponPasswordButton.addActionListener(new ActionListener() {
            //このボタンが押されるとスライダーが表示されて値を選択する。
            @Override
            public void actionPerformed(ActionEvent e) {
                // couponCodeSliderを表示するダイアログを表示
                JOptionPane.showMessageDialog(null, couponCodeSlider, "Enter Coupon Code", JOptionPane.PLAIN_MESSAGE);
            }
        });


        couponCodeConfirmButton.addActionListener(new ActionListener() {
            //このボタンが押されるとスライダーで設定した値が正しいか判定する。
            @Override
            public void actionPerformed(ActionEvent e) {
                if(couponUsageCount==-1){//クーポンが使用済みかどうかの判定
                    int sliderValue = couponCodeSlider.getValue();
                    if (sliderValue == 51) {
                        // スライダーの値が51の場合、すべての商品の価格を半額にする
                        for (String itemName : menu.keySet()) {
                            int currentPrice = menu.get(itemName);
                            int discountedPrice = currentPrice / 2;
                            menu.put(itemName, discountedPrice);
                        }
                        // 価格が変更された場合、合計金額を再計算
                        recalculateTotalPrice();
                        JOptionPane.showMessageDialog(null, "All products are now half price.", "Thank you.", JOptionPane.PLAIN_MESSAGE);
                        couponUsageCount=1;//クーポン使用済みにする。

                        //表示価格も半額にする。
                        for(int i=0;i<getProductPrice.length;i++){
                            getProductPrice[i]=getProductPrice[i]/2;  //半額にする。
                        }
                        setButtonText(menuButtons,getProductNames,getProductPrice); //半額にしたうえで再表示
                    }
                    else {
                        JOptionPane.showMessageDialog(null, "Coupon code is incorrect.", "Sorry", JOptionPane.PLAIN_MESSAGE);
                    }
                }
                else{
                    JOptionPane.showMessageDialog(null, "This coupon has already benn used..", "Can not use", JOptionPane.PLAIN_MESSAGE);
                }

            }
        });


        checkOutButton.addActionListener(new ActionListener() {
            //注文の終了を確認　yesの場合は合計金額と注文リストを初期化

            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to check out?",
                        "Check Out Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you! Please pay ￥"+totalPrice, "Check out", JOptionPane.PLAIN_MESSAGE);
                    // 注文リストと合計金額を初期化
                    orderList.clear();
                    orderedItemList.setModel(orderList);
                    totalPrice = 0;
                    totalAmountLabel.setText("Total : " + totalPrice + " yen");

                    //過去の注文数をリセットするために1で初期化
                    for (Map.Entry<String, JSpinner> entry : purchaseNumber.entrySet()) {
                        JSpinner spinner = entry.getValue();
                        spinner.setValue(1);
                    }

                    if(couponUsageCount==1){ //クーポンを使用していた場合は金額設定を元に戻す。
                        //クーポンコードを未使用に変換
                        couponUsageCount = -1;

                        //商品価格も元に戻すにする。
                        for (int i = 0; i < getProductPrice.length; i++) {
                            getProductPrice[i] = getProductPrice[i] * 2;
                        }

                        //商品価格を元に戻したうえでmenuを再更新
                        for (int i = 0; i < getProductNames.length; i++) {
                            // 商品名と価格をマップに追加
                            menu.put(getProductNames[i], getProductPrice[i]);
                        }

                        //表示金額を戻すため再設定
                        setButtonText(menuButtons, getProductNames, getProductPrice);
                    }

                }
            }
        });


        //↓商品ボタンの設定↓


        takeoutMenuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // takeoutメニューのボタン表示に切り替える処理
                showTakeoutButtons();
            }
        });

        setMenuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // setmenuメニューのボタン表示に切り替える処理
                showSetButtons();
            }
        });

        singleMenuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // singleItemメニューのボタン表示に切り替える処理
                showSingleButtons();
            }
        });

        kidsMenuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // kidsMenuメニューのボタン表示に切り替える処理
                showKidsButtons();
            }
        });

        allMenuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // kidsMenuメニューのボタン表示に切り替える処理
                showAllButtons();
            }
        });

        takeOutBeefBowlButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                quantitySelection("Beef Bowl_take out");
            }
        });

        takeOutOkraBeefBowlButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                quantitySelection("Okra Beef Bowl_take out");
            }
        });

        takeOutSukiyakiBowlButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                quantitySelection("Sukiyaki Bowl_take out");
            }
        });

        setBeefBowlButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                quantitySelection("Beef Bowl_set");
            }
        });

        setCurryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                quantitySelection("Curry Set");
            }
        });

        setKidsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                quantitySelection("Kids set");
            }
        });

        kidsYakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                quantitySelection("Kids Yakisoba");
            }
        });

        kidsCurryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                quantitySelection("Kids Curry");
            }
        });

        singleOroshiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                quantitySelection("Oroshi Ponzu Beef Bowl_single");
            }
        });

        singleChickenCurryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                quantitySelection("Chicken Curry_single");
            }
        });

        singleBeefBowlButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                quantitySelection("Beef Bowl_single");
            }
        });
    }


    private void quantitySelection(String foodName) {
        //商品のボタンが押された後に購入数を設定するメソッド

        JSpinner  temQuantity= purchaseNumber.get(foodName);
        //purchaseNumberに格納されているJSpinner型の要素を取り出している。
        //JSpinnerはユーザが決定した購入数を表す。もし以前に注文されている場合はその注文数が格納されている。

        int result = JOptionPane.showOptionDialog(
                null,
                temQuantity,
                "Select Quantity for " + foodName,
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.PLAIN_MESSAGE,
                null,
                null,
                null);

        if (result == 0) { //注文が確定したら
            int quantity = (int) temQuantity.getValue();//確定された商品数を代入
            updateOrder(foodName, quantity);            //履歴と合計金額をアップデートする。
        }
    }


    private void updateOrder(String foodName, int quantity) {
        //商品を選択されたあと、注文個数の確認と確定のお知らせを表示するメソッド
        //注文個数quantityも引数に取る。
        //もし注文済の商品数の変更がある時に、過去の注文履歴を消去して
        //合計金額も過去に注文された商品の分をリセットしてから再度追加する。

        int existingOrderIndex = -1;
        //過去に同一商品の注文が存在していたかを確認する変数
        //-1:存在していないことを表す。存在していた場合、orderListの何行目に存在しているかを記憶

        // 以前の注文があるか確認
        for (int i = 0; i < orderList.size(); i++) {   //orderList.size():表示されている行数を取得
            if (orderList.getElementAt(i).startsWith(foodName)) {
                //orderListのi行目の文字列を取得し、それが文字列foodNameで始まるかを判定する。

                existingOrderIndex = i;
                //もしfoodNameで始まる文字列が存在する場合は何行目にそれが存在したかを記憶
                break;
            }
        }

        if (existingOrderIndex != -1) {
            // 同じ商品が存在するので、注文を更新

            String existingOrder = orderList.getElementAt(existingOrderIndex);
            //existingOrderIndexには重複している注文が存在する行番号が含まれるのでその行の内容を取得してexistingOrderに代入

            int existingQuantity = extractQuantity(existingOrder);
            //取得した文字列から注文した数量の情報を抽出してくれるメソッドに渡す。foodName x3 →3 のように抽出

            orderList.removeElementAt(existingOrderIndex);
            // 注文リストから既存の注文を削除

            totalPrice -= menu.get(foodName) * existingQuantity;
            // 合計金額から既存の注文の金額を減算

            // 更新した注文をリストに追加して画面に表示する。
            int newTotalQuantity = quantity;
            String updatedOrderDescription = foodName + " x" + newTotalQuantity;
            orderList.addElement(updatedOrderDescription);

            // 合計金額を更新
            totalPrice += menu.get(foodName) * newTotalQuantity;
            totalAmountLabel.setText("Total : " + totalPrice + " yen");
        }

        else {
            // 同じ商品が存在しない場合、新しい注文を追加
            String orderDescription = foodName + " x" + quantity;
            orderList.addElement(orderDescription);

            // 合計金額を更新
            totalPrice += menu.get(foodName) * quantity;
            totalAmountLabel.setText("Total : " + totalPrice + " yen");
        }

        //0個に設定された場合、orderListに表示する必要がないので、これを削除する。
        //更新したfoodNameは必ず最終行に追加されているので.lastElementで取り出す。
        //この行から注文数をメソッドで抽出して、0個であれば取り除く
        if(extractQuantity(orderList.lastElement()) == 0){
            orderList.remove(orderList.getSize()-1);
        }
    }


    private int extractQuantity(String order) {
        // 文字列orderに含まれる注文数の情報を抽出

        String[] parts = order.split("x");
        //受け取った文字列はfoodName+" x"+quantityの形である。
        //これを"x"で分割して、各部分を配列partsに格納

        return Integer.parseInt(parts[1].trim());
        //parts[0]=foodName,parts[1]=quantity
        //.trimは文字列の両端の不要な空白を削除する。また文字列型なので整数型にInteger.parseIntで変換している。
    }


    private void recalculateTotalPrice() {
        // 合計金額を再計算して表示
        totalPrice = 0;
        for (int i = 0; i < orderList.size(); i++) {
            String itemName = orderList.getElementAt(i);//i行目の内容を取得foodName x3といった形式のもの
            int quantity = extractQuantity(itemName);   //メソッドを呼び出し上のコメントの例でいえば3を抽出
            String[] parts = itemName.split(" x");//" x"で文字列を区切ってpartsに格納
            if (parts.length >= 2) {                    //一応要素が二つ以上存在することを確認
                String foodName = parts[0];             //foodNameを呼び出して、その値段、数量で合計金額を再計算
                totalPrice += menu.get(foodName) * quantity;
            }
        }
        totalAmountLabel.setText("Total : " + totalPrice + " yen");  //合計金額をの表示を更新
    }


    private void setAndResizeAllImageIcon(JButton[] button, String[] iconName, int iconSize) {
        //buttonにsize平方の大きさに調整をされたIconNameの名前を持つ画像をを設定するメソッド

        for(int i=0;i< button.length;i++) {
            // 指定されたアイコン名を読み込む
            ImageIcon originalIcon = new ImageIcon(this.getClass().getResource(iconName[i]));

            // 元の画像アイコンからイメージを取得
            Image originalImage = originalIcon.getImage();

            // 新しいサイズに画像をリサイズ
            Image resizedImage = originalImage.getScaledInstance(iconSize, iconSize, Image.SCALE_SMOOTH);

            // リサイズされた画像を新しいImageIconに設定
            ImageIcon resizedIcon = new ImageIcon(resizedImage);

            // ボタンに画像を設定する
            button[i].setIcon(resizedIcon);
        }
    }

    private void setButtonText(JButton[] button,String[] itemNames,Integer[] itemPrice){
        for(int i=0;i<itemNames.length;i++){
            button[i].setText(itemNames[i]+" ￥"+itemPrice[i]);
        }
    }


    //以下の四つのメソッドshow ~ Buttonsは ~ ボタンが押された際にそのグループの商品が表示されるようにGUIを変更する
    private void showTakeoutButtons() {
        takeOutBeefBowlButton.setVisible(true);
        takeOutOkraBeefBowlButton.setVisible(true);
        takeOutSukiyakiBowlButton.setVisible(true);
        setBeefBowlButton.setVisible(false);
        setKidsButton.setVisible(false);
        setCurryButton.setVisible(false);
        kidsCurryButton.setVisible(false);
        kidsYakisobaButton.setVisible(false);
        singleBeefBowlButton.setVisible(false);
        singleChickenCurryButton.setVisible(false);
        singleOroshiButton.setVisible(false);
    }

    private void showSetButtons() {
        takeOutBeefBowlButton.setVisible(false);
        takeOutOkraBeefBowlButton.setVisible(false);
        takeOutSukiyakiBowlButton.setVisible(false);
        setBeefBowlButton.setVisible(true);
        setKidsButton.setVisible(true);
        setCurryButton.setVisible(true);
        kidsCurryButton.setVisible(false);
        kidsYakisobaButton.setVisible(false);
        singleBeefBowlButton.setVisible(false);
        singleChickenCurryButton.setVisible(false);
        singleOroshiButton.setVisible(false);
    }

    private void showSingleButtons() {
        takeOutBeefBowlButton.setVisible(false);
        takeOutOkraBeefBowlButton.setVisible(false);
        takeOutSukiyakiBowlButton.setVisible(false);
        setBeefBowlButton.setVisible(false);
        setKidsButton.setVisible(false);
        setCurryButton.setVisible(false);
        kidsCurryButton.setVisible(false);
        kidsYakisobaButton.setVisible(false);
        singleBeefBowlButton.setVisible(true);
        singleChickenCurryButton.setVisible(true);
        singleOroshiButton.setVisible(true);
    }

    private void showKidsButtons() {
        takeOutBeefBowlButton.setVisible(false);
        takeOutOkraBeefBowlButton.setVisible(false);
        takeOutSukiyakiBowlButton.setVisible(false);
        setBeefBowlButton.setVisible(false);
        setKidsButton.setVisible(false);
        setCurryButton.setVisible(false);
        kidsCurryButton.setVisible(true);
        kidsYakisobaButton.setVisible(true);
        singleBeefBowlButton.setVisible(false);
        singleChickenCurryButton.setVisible(false);
        singleOroshiButton.setVisible(false);
    }

    private void showAllButtons(){
        takeOutBeefBowlButton.setVisible(true);
        takeOutOkraBeefBowlButton.setVisible(true);
        takeOutSukiyakiBowlButton.setVisible(true);
        setBeefBowlButton.setVisible(true);
        setKidsButton.setVisible(true);
        setCurryButton.setVisible(true);
        kidsCurryButton.setVisible(true);
        kidsYakisobaButton.setVisible(true);
        singleBeefBowlButton.setVisible(true);
        singleChickenCurryButton.setVisible(true);
        singleOroshiButton.setVisible(true);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderMachine");
        frame.setContentPane(new FoodOrderMachine().root);
        FoodOrderMachine foodOrderMachine = new FoodOrderMachine();
        JScrollPane scrollPane = new JScrollPane(foodOrderMachine.root);
        frame.setContentPane(scrollPane);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
